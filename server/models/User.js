const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: { type: String, unique: true },
	password: String
});

userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
	const isMatch = candidatePassword === this.password;
	const err = !isMatch && new Error('Invalid password');
	cb(err, isMatch);
};

const User = mongoose.model('User', userSchema);

module.exports = User;
