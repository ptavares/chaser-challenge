const socketIo = require('socket.io');
module.exports.init = function(server) {
	module.exports.io = socketIo(server);
}
