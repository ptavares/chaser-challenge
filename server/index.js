const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const errorHandler = require('errorhandler');
const passport = require('passport');
const expressValidator = require('express-validator');
const dotenv = require('dotenv');
const frontendMiddleware = require('./middlewares/frontendMiddleware');

// load environment variables from .env file
dotenv.load({ path: '.env.example' });

// controllers
const userController = require('./controllers/user');
const postController = require('./controllers/post');

// passport config
const passportConfig = require('./config/passport');
const isAuthenticated = passportConfig.isAuthenticated;
// create express server
const app = express();

// connect to MongoDB
mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);
mongoose.connection.on('error', () => {
  console.log('MongoDB connection error. Please make sure MongoDB is running.');
  process.exit();
});

// express app configuration
app.set('port', process.argv.port || process.env.PORT || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: process.env.MONGODB_URI || process.env.MONGOLAB_URI,
    autoReconnect: true
  })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Credentials', 'true');
	next();
})
app.post('/login', userController.postLogin);
app.post('/signup', userController.postSignup);
app.get('/context', isAuthenticated, userController.getContext);
app.get('/post', postController.getPosts);
app.post('/post', isAuthenticated, postController.postPost);
app.post('/like', isAuthenticated, postController.postLike);
app.post('/reply', isAuthenticated, postController.postReply);

// error handler
app.use(errorHandler());

// In production we need to pass these values in instead of relying on webpack
frontendMiddleware(app, {
  outputPath: require('path').resolve(process.cwd(), 'build'),
  publicPath: '/',
});

const httpServer = require('http').createServer(app);
require('./io').init(httpServer);

// start express server.
httpServer.listen(app.get('port'), () => {
  console.log(`App is running at http://localhost:${app.get('port')} in ${app.get('env')} mode`); 
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
