const Post = require('../models/Post');

exports.postPost = (req, res, next) => {
	req.assert('text', 'Text cannot be empty').notEmpty();

	const errors = req.validationErrors();

  if (errors) {
    return res.status(400).json(errors);
  }

	const post = new Post({
		text: req.body.text,
		postedBy: req.user
	});

	post.save((err) => {
		if (err) { return next(err); }
		return res.status(201).json({ msg: 'Success' });
	});
};

exports.postLike = (req, res, next) => {
	req.assert('postId', 'Post ID cannot be empty').notEmpty();

	const errors = req.validationErrors();

  if (errors) {
    return res.status(400).json(errors);
  }

	Post.findById(req.body.postId, (err, post) => {
		post.likedBy.push(req.user);
		post.save();
		return res.status(200).json({});
  });
}

exports.postReply = (req, res, next) => {
	req.assert('postId', 'Post ID cannot be empty').notEmpty();
	req.assert('text', 'Text cannot be empty').notEmpty();

	const errors = req.validationErrors();
  if (errors) {
    return res.status(400).json(errors);
  }

	Post.findById(req.body.postId)
		.populate('postedBy')
		.exec((err, post) => {
			const comment = {
				text: req.body.text,
				postedBy: req.user
			}
			post.comments.push(comment);
			post.save();
			const io = require('../io').io;
			io.emit('reply', {
				postId: req.body.postId,
				originalPoster: post.postedBy.email
			});
			return res.status(200).json({});
	  });
}

exports.getPosts = (req, res) => {
	Post.find({})
		.populate('postedBy')
		.populate('likedBy')
		.populate('comments.postedBy')
		.sort({'_id': -1})
		.exec((err, posts) => {
			if (err) { return next(err); }
			return res.status(200).json(posts);
		});
};
