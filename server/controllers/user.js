const passport = require('passport');
const User = require('../models/User');

exports.postLogin = (req, res, next) => {
	req.assert('email', 'Email is not valid').isEmail();
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.sanitize('email').normalizeEmail({ remove_dots: false });

	const errors = req.validationErrors();

  if (errors) {
    return res.status(400).json(errors);
  }

	passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      return res.status(400).json({ msg: 'User not found' });
    }
    req.logIn(user, (err) => {
      if (err) { return next(err); }
			res.header('Access-Control-Allow-Credentials', 'true');
      return res.status(200).json({ msg: 'Success' });
    });
  })(req, res, next);
};

exports.postSignup = (req, res, next) => {
	req.assert('email', 'Email is not valid').isEmail();
	req.assert('password', 'Password must be at least 4 characters long').len(4);
	req.assert('confirmPassword', 'Passwords do not match').equals(req.body.password);
	req.sanitize('email').normalizeEmail({ remove_dots: false });

	const errors = req.validationErrors();

	if (errors) {
    return res.status(400).json(errors);
  }

	const user = new User({
    email: req.body.email,
    password: req.body.password
  });

	User.findOne({ email: req.body.email }, (err, existingUser) => {
    if (err) { return next(err); }
    if (existingUser) {
      return res.status(400).json({ msg: 'Account with that email address already exists.' });
    }
    user.save((err) => {
      if (err) { return next(err); }
      req.logIn(user, (err) => {
        if (err) {
          return next(err);
        }
        res.status(201).json({ msg: 'Success'});
      });
    });
  });
};

exports.getContext = (req, res, next) => {
	res.json(req.user);
}
