import {
	REQUEST_POSTS,
  RECEIVE_POSTS,
} from './constants';

function requestPosts() {
	return {
    type: REQUEST_POSTS
  }
}

function receivePosts(posts) {
	return {
    type: RECEIVE_POSTS,
    posts: posts
  }
}
