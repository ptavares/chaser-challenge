/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHome = () => (state) => state.get('home');

const selectUsername = () => createSelector(
  selectHome(),
  (homeState) => homeState.get('username')
);

const selectPosts = () => (state) => {
	return state.get('posts');
};

export {
  selectHome,
  selectUsername,
	selectPosts
};
