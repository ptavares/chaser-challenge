/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import Helmet from 'react-helmet';
import AppBar from '../../components/AppBar';
import AddPostCard from '../../components/AddPostCard';
import PostCard from '../../components/PostCard';
import NotificationsEnabler from '../../components/NotificationsEnabler';

import { createStructuredSelector } from 'reselect';

import {
	selectPosts,
	selectEmail,
} from '../App/selectors';

import {
  selectUsername,
} from './selectors';

import {
	fetchPosts,
	login,
	destroySession,
	register,
	submitPost,
	like,
	reply,
} from '../App/actions';

import { FormattedMessage } from 'react-intl';

import styles from './styles.css';

export class HomePage extends React.Component {

  componentDidMount() {
		this.props.onCreate();
  };
  /**
   * Changes the route
   *
   * @param  {string} route The route we want to go to
   */
  openRoute = (route) => {
    this.props.changeRoute(route);
  };

  render() {
		const isLoggedIn = !!this.props.email;

		const notificationEnabler = Notification.permission !== "granted" && (
			<NotificationsEnabler/>
		);

		const addPostCard = isLoggedIn && (<AddPostCard onSubmitClicked={this.props.onSubmitClicked} />);
		const postCards = this.props.posts.map((post) =>
			<span>
			<br />
			<PostCard
				style={{marginTop: 10}}

				post={post}
				email={this.props.email}
				onPostLiked={this.props.onPostLiked}
				onReplySubmited={this.props.onReplySubmited}
			/>
			</span>);
    return (
      <article>
				<AppBar
					logged={isLoggedIn}
					onLoginClicked={this.props.onLoginClicked}
					onRegisterClicked={this.props.onRegisterClicked}
					onLogOutClicked={this.props.onLogOutClicked}
				/>
				{notificationEnabler}
        <Helmet
          title="Home Page"
          meta={[
            { name: 'description', content: 'Chaser Challenge Homepage' },
          ]}
        />
				<div className={styles.wrapper}>
					{addPostCard}
					{React.Children.toArray(postCards)}
				</div>
      </article>
    );
  }
}

HomePage.propTypes = {
	posts:  React.PropTypes.array,
	user:  React.PropTypes.object,
	onLoginClicked: React.PropTypes.func,
	onRegisterClicked: React.PropTypes.func,
	onLogOutClicked: React.PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
		onCreate: () => dispatch(fetchPosts()),
		onLoginClicked: (email, password) => dispatch(login(email, password)),
		onRegisterClicked: (email, password, confirmPassword) => dispatch(register(email, password, confirmPassword)),
		onLogOutClicked: () => dispatch(destroySession()),
		onSubmitClicked: (text) => dispatch(submitPost(text)),
		onPostLiked: (id) => dispatch(like(id)),
		onReplySubmited: (id, text) => dispatch(reply(id, text)),
		initSocket: () => dispatch(initSocket()),
    dispatch,
  };
}

const mapStateToProps = createStructuredSelector({
	posts: selectPosts(),
	email: selectEmail(),
});

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
