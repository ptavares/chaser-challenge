/*
 * App Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
	RECEIVE_POSTS,
	LOGIN,
	LOGOUT,
} from './constants';

import {
	selectEmail
} from './selectors';

//import io from 'socket.io-client';
/**
 * Dispatched when the posts are received
 *
 * @param  {array} posts 	The posts
 *
 * @return {object}       An action object with a type of RECEIVE_POSTS passing the posts
 */
export function receivePosts(posts) {
	return {
    type: RECEIVE_POSTS,
    posts: posts
  }
}

/**
 * Dispatched when the user session is created
 *
 * @param  {array} posts 	The posts
 *
 * @return {object}       An action object with a type of RECEIVE_POSTS passing the posts
 */
export function createSession(email) {
	return {
		type: LOGIN,
		email
	}
}

/**
 * Dispatched when the user session is created
 *
 * @param  {array} posts 	The posts
 *
 * @return {object}       An action object with a type of RECEIVE_POSTS passing the posts
 */
export function destroySession() {
	return {
		type: LOGOUT
	}
}

export function login(email, password) {
	return function (dispatch) {
		return fetch(`/login`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ email, password })
		}).then((response) => response.json())
			.then(json =>
				dispatch(createSession(email)));
	}
}

export function register(email, password, confirmPassword) {
	return function (dispatch) {
		return fetch(`/signup`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ email, password, confirmPassword })
		}).then((response) => response.json())
			.then(json =>
				dispatch(createSession(email)));
	}
}

export function submitPost(text) {
	return function (dispatch) {
		return fetch(`/post`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ text })
		}).then(json =>
			dispatch(fetchPosts())
		);
	}
}

export function fetchPosts() {
	return function (dispatch) {
		return fetch(`/post`)
      .then(response => response.json())
      .then(json =>
        dispatch(receivePosts(json))
      )
	}
}

export function like(postId) {
	return function (dispatch) {
		return fetch(`/like`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ postId })
		});
	}
}

export function reply(postId, text) {
	return function (dispatch) {
		return fetch(`/reply`, {
			method: 'POST',
			credentials: 'include',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ postId, text })
		}).then(json =>
			dispatch(fetchPosts())
		);
	}
}
