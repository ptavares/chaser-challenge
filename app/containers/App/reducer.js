/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import {
	RECEIVE_POSTS,
	LOGIN,
	LOGOUT,
	SOCKET_MESSAGE
} from './constants';
import { fromJS } from 'immutable';

import {selectEmail} from './selectors';

// The initial state of the App
const initialState = fromJS({
	posts: [],
	email: undefined
});

function appReducer(state = initialState, action) {
  switch (action.type) {
		case RECEIVE_POSTS:
			return state.set('posts', fromJS(action.posts));
		case LOGIN:
			return state.set('email', fromJS(action.email));
		case LOGOUT:
			return state.set('email', undefined);
		case SOCKET_MESSAGE:
			new Notification("New reply!");
			return state;
    default:
      return state;
  }
}

export default appReducer;
