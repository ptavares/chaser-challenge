export const RECEIVE_POSTS = 'chaser_challenge/App/RECEIVE_POSTS';
export const LOGIN = 'chaser_challenge/App/LOGIN';
export const LOGOUT = 'chaser_challenge/App/LOGOUT';
export const SOCKET_MESSAGE = 'chaser_challenge/App/SOCKET_MESSAGE';
