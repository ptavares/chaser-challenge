import React from 'react';
import Helmet from 'react-helmet';

// Import the CSS reset, which HtmlWebpackPlugin transfers to the build folder
import 'sanitize.css/sanitize.css';

function App(props) {
	return (
		<div>
			<Helmet
        titleTemplate="%s - Chaser Challenge"
        defaultTitle="Chaser Challenge"
        meta={[
          { name: 'description', content: 'Chaser Challenge' },
        ]}
      />
			{React.Children.toArray(props.children)}
		</div>
	);
}

App.propTypes = {
  children: React.PropTypes.node,
};

export default App;
