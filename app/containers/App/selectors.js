/**
 * The global state selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = () => (state) => state.get('global');

const selectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

const selectPosts = () => createSelector(
  selectGlobal(),
  (globalState) => globalState.get('posts').toJS()
);

const selectEmail = () => createSelector(
  selectGlobal(),
  (globalState) =>  globalState.get('email')
);

export {
  selectGlobal,
  selectLocationState,
	selectPosts,
	selectEmail,
};
