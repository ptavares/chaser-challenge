import {
	SOCKET_MESSAGE
} from '../containers/App/constants';

export default function initSocket(store) {
	const socket = io.connect();
	socket.on('reply', (data) => {
		store.dispatch({type: SOCKET_MESSAGE, data});
	});
}
