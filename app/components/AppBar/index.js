import React, {Component} from 'react';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import Toggle from 'material-ui/Toggle';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import LoginDialog from '../LoginDialog';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

import Dialog from 'material-ui/Dialog';

import styles from './styles.css';

class Login extends Component {
  static muiName = 'FlatButton';

  render() {
    return (
      <FlatButton {...this.props} label="Login" />
    );
  }
}

const Logged = (props) => (
	<span className={styles.loggedOptions}>
	  <IconMenu
	    iconButtonElement={
	      <IconButton><MoreVertIcon /></IconButton>
	    }
	    targetOrigin={{horizontal: 'right', vertical: 'top'}}
	    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
	  >
	    <MenuItem primaryText="Sign out" onClick={props.onLogOutClicked} />
	  </IconMenu>
	</span>
);

Logged.muiName = 'IconMenu';

/**
 * This example is taking advantage of the composability of the `AppBar`
 * to render different components depending on the application state.
 */
 class AppBarExampleComposition extends Component {

	 constructor(props) {
		 super(props);
		 this.state = { isloginDialogOpen: false };
	 }

	 openLoginDialog() {
		 this.setState({ isloginDialogOpen: true });
	 }

	 onRequestClose() {
		 this.setState({ isloginDialogOpen: false });
	 }

	 isDialogOpen() {
		 if (this.props.logged) {
			 this.setState({ isloginDialogOpen: false });
			 return false;
		 }

		 return this.state.isloginDialogOpen;
	 }

	 render() {
		 return (
	 		<div>
	 			<LoginDialog
					open={this.state.isloginDialogOpen && !this.props.logged}
					onLoginClicked={this.props.onLoginClicked}
					onRegisterClicked={this.props.onRegisterClicked}
					onRequestClose={() => this.onRequestClose()}
				/>
	 			<AppBar
	 				title="Simpler Twitter"
	 				iconElementLeft={<span></span>}
	 				iconElementRight={this.props.logged ?
						<Logged onLogOutClicked={this.props.onLogOutClicked} /> :
						<Login onClick={() => this.openLoginDialog()}/>}
	 			/>
	 		</div>
	 	);
	 }
 }

AppBarExampleComposition.propTypes = {
	logged: React.PropTypes.bool,
	onLoginClicked: React.PropTypes.func,
	onRegisterClicked: React.PropTypes.func,
	onLogOutClicked:  React.PropTypes.func,
}

export default AppBarExampleComposition;
