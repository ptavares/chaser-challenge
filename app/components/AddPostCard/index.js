import React, {Component} from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

class AddPostCard extends Component {

	constructor(props) {
		super(props);
		this.state = { text: '' };
	}

	onTextFieldChanged(evt) {
		this.setState({ text: evt.target.value });
	}

	onSubmitClicked() {
		this.props.onSubmitClicked(this.state.text);
		this.setState({text: ''})
	}

	render() {
		return (
			<Card>
		    <CardTitle title="Add new post"/>
		    <CardText>
				<TextField
					multiLine={true}
					rows={1}
					fullWidth={true}
					id={"add_post"}
					maxLength="200"
					value={this.state.text}
					onChange={(evt) => this.onTextFieldChanged(evt)}
				/>
		    </CardText>
		    <CardActions>
		      <RaisedButton
						label="Submit"
						secondary={true}
						onClick={() => this.onSubmitClicked()}
					/>
		    </CardActions>
		  </Card>
		);
	}
}

AddPostCard.propTypes = {
	onSubmitClicked: React.PropTypes.func
}

export default AddPostCard;
