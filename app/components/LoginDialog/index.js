import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Divider from 'material-ui/Divider';

class LoginDialog extends Component {
	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: '',
			registerEmail: '',
			registerPassword: '',
			registerConfirmPassword: ''
		};
	}

	onTextFieldChanged(field, evt) {
		this.setState({ [field]: evt.target.value });
	}

	render() { //
		return (
			<Dialog open={this.props.open} onRequestClose={this.props.onRequestClose}>
				<h3>Login</h3>
				<TextField
					id={"login_email"}
					fullWidth={true}
					floatingLabelText={"Email"}
					onChange={(evt) => this.onTextFieldChanged('email', evt)}
				/>
				<br/>
				<TextField
					id={"login_password"}
					fullWidth={true}
					floatingLabelText={"Password"}
					type="password"
					onChange={(evt) => this.onTextFieldChanged('password', evt)}
				/>
				<br />
				<RaisedButton label="Login" secondary={true} onClick={() => this.props.onLoginClicked('rick@rick.com', '1234')}/>
				<br /><br />
				<Divider />
				<h3>Register</h3>
				<TextField
					id={"register_email"}
					fullWidth={true}
					floatingLabelText={"Email"}
					onChange={(evt) => this.onTextFieldChanged('registerEmail', evt)}
				/>
				<br/>
				<TextField
					id={"register_password"}
					fullWidth={true}
					floatingLabelText={"Password"}
					type="password"
					onChange={(evt) => this.onTextFieldChanged('registerPassword', evt)}
				/>
				<br />
				<TextField
					id={"register_confirm_password"}
					fullWidth={true}
					floatingLabelText={"Confirm Password"}
					type="password"
					onChange={(evt) => this.onTextFieldChanged('registerConfirmPassword', evt)}
				/>
				<br />
				<RaisedButton label="Register" secondary={true} onClick={() =>
					this.props.onRegisterClicked(
						this.state.registerEmail,
						this.state.registerPassword,
						this.state.registerConfirmPassword
					)}
				/>
			</Dialog>
		)
	}
}

LoginDialog.propTypes = {
	open: React.PropTypes.bool,
	onLoginClicked: React.PropTypes.func,
	onRegisterClicked: React.PropTypes.func,
	onRequestClose: React.PropTypes.func
}

export default LoginDialog;
