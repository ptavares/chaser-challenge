import React, {Component} from 'react';
import {Card, CardActions, CardTitle, CardText} from 'material-ui/Card';
import Divider from 'material-ui/Divider';
import Toggle from 'material-ui/Toggle';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class PostCard extends Component {

	constructor(props) {
		super(props);

		this.state = { reply: '' }
	}

	onToggle(val, value) {
		const { _id: id } = this.props.post;
		this.props.onPostLiked(id);
	}

	onReplyChanged(evt) {
		this.setState({ reply: evt.target.value });
	}

	render() {
		const toggled = this.props.post.likedBy.some((likers) => likers.email === this.props.email);
		const { _id, text, likedBy, postedBy: { email }, comments  } = this.props.post; //
		const numberOfLikes = likedBy.length;

		const isLogged = !!this.props.email;
		const toggle = isLogged && (
			<span>
			Like: <Toggle toggled={toggled} onToggle={(evt, value) => this.onToggle(evt, value)} />
			</span>
		);
		const reply = isLogged && (
			<span>
			<Divider />
			<TextField
				rows={1}
				fullWidth={true}
				id={"reply"}
				placeholder={"Write a reply"}
				onChange={(evt) => this.onReplyChanged(evt)}
			/>
			<RaisedButton
				label="Submit reply"
				onClick={() => this.props.onReplySubmited(_id, this.state.reply)}
			/>
			</span>
		);
		const replies = comments.map((comment) => {
			const author = comment.postedBy.email;
			const text = comment.text;
			return (
				<span>
					<p>{text}</p>
					<small>by {author}</small>
					<Divider/>
				</span>
			);
		});

		return (
			<Card>
				<CardText>
				{text}
				<br/>
				by {email}
				<Divider />
				{toggle}
				Liked by { numberOfLikes }
				{reply}
				{React.Children.toArray(replies)}
				</CardText>
				<Divider />
			</Card>
		);
	}
}

PostCard.propTypes = {
	post: React.PropTypes.object.isRequired,
	email:  React.PropTypes.string,
	onPostLiked: React.PropTypes.func,
	onReplySubmited: React.PropTypes.func,
}

export default PostCard;
