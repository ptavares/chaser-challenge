import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const NotificationsEnabler = () => {
	const enableNotifications = () => {
		Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Hi there!");
      }
    });
	}
	return (
		<RaisedButton
			label="Enable Notifications"
			onClick={enableNotifications}
		/>
	);
}

export default NotificationsEnabler;
