// App entry point

// Needed for redux-saga es6 generator support
import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { applyRouterMiddleware, Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import FontFaceObserver from 'fontfaceobserver';
import { useScroll } from 'react-router-scroll';
import configureStore from './store';

import styles from './containers/App/styles.css';
const openSansObserver = new FontFaceObserver('Open Sans', {});

import initSocket from './io';

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add(styles.fontLoaded);
}, () => {
  document.body.classList.remove(styles.fontLoaded);
});

import { translationMessages } from './i18n';

import { selectLocationState } from './containers/App/selectors';
import App from './containers/App';
import createRoutes from './routes';

import injectTapEventPlugin from 'react-tap-event-plugin';
//injectTapEventPlugin();

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'


const initialState = {};
configureStore(initialState, browserHistory)
	.then((store) => {
		initSocket(store);

		if (window.devToolsExtension) {
		  window.devToolsExtension.updateStore(store);
		}

		const history = syncHistoryWithStore(browserHistory, store, {
		  selectLocationState: selectLocationState(),
		});

		const rootRoute = {
		  component: App,
		  childRoutes: createRoutes(store),
		};

		const lightMuiTheme = getMuiTheme(lightBaseTheme);

		const render = () => {
		  ReactDOM.render(
		    <Provider store={store}>
					<MuiThemeProvider muiTheme={lightMuiTheme}>
			      <Router
			        history={history}
			        routes={rootRoute}
			        render={
			          // Scroll to top when going to a new page, imitating default browser
			          // behaviour
			          applyRouterMiddleware(useScroll())
			        }
			      />
					</MuiThemeProvider>
		    </Provider>,
		    document.getElementById('app')
		  );
		};


		// Hot reloadable translation json files
		if (module.hot) {
		  // modules.hot.accept does not accept dynamic dependencies,
		  // have to be constants at compile-time
		  module.hot.accept('./i18n', () => {
		    render(translationMessages);
		  });
		}

		// Chunked polyfill for browsers without Intl support
		if (!window.Intl) {
		  (new Promise((resolve) => {
		    resolve(System.import('intl'));
		  }))
		    .then(() => Promise.all([
		      System.import('intl/locale-data/jsonp/en.js'),
		      System.import('intl/locale-data/jsonp/pt.js'),
		    ]))
		    .then(() => render(translationMessages))
		    .catch((err) => {
		      throw err;
		    });
		} else {
		  render(translationMessages);
		}

	});
